import tkinter
from tkinter import *
import time
import Reversi

SIZE_WINDOW = 700
SIZE_BOX = SIZE_WINDOW/10
SIZE_PIECE = SIZE_BOX - 2
MARGIN = SIZE_BOX - SIZE_PIECE


class Gui :

    def __init__(self):
        self._board = Reversi.Board(10)
        self._root = tkinter.Tk()
        self._canvas = tkinter.Canvas(self._root,width = SIZE_WINDOW, height = SIZE_WINDOW, background="green")
        self._canvas.pack()
        self._root.wm_title("Reversi")
        

    def grid(self) :
        for i in range (self._board.get_board_size()):
            self._canvas.create_line(0,i*SIZE_BOX,SIZE_WINDOW,i*SIZE_BOX, width = 2)
            self._canvas.create_line(i*SIZE_BOX,0,i*SIZE_BOX,SIZE_WINDOW, width = 2)
        self._canvas.update()

        
    def draw(self,board):
        for x in range (self._board.get_board_size()):
            for y in range (self._board.get_board_size()):
                if(board[x][y] != 0):
                    if board[x][y] == 1:
                        color = 'black'                              
                    elif board[x][y]==2:
                        color = 'white'                
                    self._canvas.create_oval(MARGIN +SIZE_BOX*y, MARGIN +SIZE_BOX*x,SIZE_PIECE+SIZE_BOX*y,SIZE_PIECE+SIZE_BOX*x,fill=color, outline="")               
        self._canvas.update()

    def wait(self):
        self._root.mainloop()