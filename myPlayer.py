# -*- coding: utf-8 -*-
import time
import Reversi
import math
import numpy as np
from random import randint, choice
from playerInterface import *


class myPlayer(PlayerInterface):

    timeTurn =0
    myNbMoves = 0
    oppNbMoves = 0
    totalPieces = 0

    def __init__(self):
        self._board = Reversi.Board(10)
        self._mycolor = None
        

    def getPlayerName(self):
        return "SeynShimfu"

    def getPlayerMove(self):
        if self._board.is_game_over():
            print("Referee told me to play but the game is over!")
            return (-1,-1)
        
        startTime = time.time()
        
        print("MY TIMER :",self.timeTurn)
        print("I HAVE",self.myNbMoves,"LEGAL MOVES")
        print("OPPONENT HAVES", self.oppNbMoves, "LEGAL MOVES")

        if self.totalPieces < 40:
            print("Evaporation Strategy")
        else:
            print("Maximum Strategy")
        move = self.bestmove(2)
        
        self._board.push(move)
        print("I am playing ", move)
        (c,x,y) = move
        
        assert(c==self._mycolor)
        print("My current board :")
        print(self._board)
        endTime = time.time()
        self.timeTurn+= endTime - startTime
        return (x,y) 

    def playOpponentMove(self, x,y):
        assert(self._board.is_valid_move(self._opponent, x, y))
        print("Opponent played ", (x,y))
        self._board.push([self._opponent, x, y])

    def newGame(self, color):
        
        self._mycolor = color
        print("COLOR",color)
        self._opponent = 1 if color == 2 else 2

    def endGame(self, winner):
        if self._mycolor == winner:
            print("I won!!!")
        else:
            print("I lost :(!!")

    def bestmove(self,maxdepth):
        best = -math.inf
        v = -math.inf       
        for move in self._board.legal_moves():            
            self._board.push(move)
            v = max(v,self.negascout(maxdepth,-math.inf,math.inf))
            if v > best:
                best = v
                best_move = move
            self._board.pop()
        return best_move


    global Square
    Square = np.array([
        [500,-150,75,30,30,30,30,75,-150,500],
        [-150,-250,-50,-50,-50,-50,-50,-50,-250,-150],
        [75,-50,100,0,0,0,0,100,-50,75],
        [30,-50,0,10,20,20,10,0,-50,30],
        [30,-50,0,20,0,0,20,0,-50,30],
        [30,-50,0,20,0,0,20,0,-50,30],
        [30,-50,0,10,20,20,10,0,-50,30],
        [75,-50,100,0,0,0,0,100,-50,75],
        [-150,-250,-50,-50,-50,-50,-50,-50,-250,-150],
        [500,-150,75,30,30,30,30,75,-150,500]
        ])

    def evaluation(self, player=None):
 
        score = 0
        if self._mycolor == 1:
            myPiece = self._board._BLACK
            OpponentPiece = self._board._WHITE
            myNbPieces = self._board._nbBLACK
            OpponentNbPieces = self._board._nbWHITE

        if self._mycolor == 2:
            myPiece = self._board._WHITE
            OpponentPiece = self._board._BLACK
            myNbPieces = self._board._nbWHITE
            OpponentNbPieces = self._board._nbBLACK

        # Position strategy
        for x in range(self._board.get_board_size()):
            for y in range(self._board.get_board_size()):
                if self._board._board[x][y] == myPiece:
                    score += Square[x,y]
                elif self._board._board[x][y] == OpponentPiece:
                    score -= Square[x,y]

        #Stability strategy        
        if self._board._board[0][0] != 0 and Square[0,1]== -150:
            for i in range(1,9,2):
                Square[0,i] = 175
                Square[i,0] = 175         
            Square[1,1] = 175
        if self._board._board[0][9] != 0 and Square[8,0] == -150 :
            for i in range(1,9,2):
                Square[9,i] = 175
            for i in range(8,0,-2):
                Square[i,0] = 175           
            Square[8,1] = 175
        if self._board._board[9][0] != 0 and Square[1,9] == -150 :
            for i in range(1,9,2):
                Square[i,9] = 175
            for i in range(8,0,-2):
                Square[0,i] = 175
            Square[8,1] = 175
        if self._board._board[9][9] != 0 and Square[9,8] == -150 :
            for i in range(8,0,-2):
                Square[9,i] = 175
                Square[i,9] = 175
            Square[8,8] = 175

        # Evaporation strategy
        if (myNbPieces + OpponentNbPieces <40):
            score += 70*(OpponentNbPieces - myNbPieces)

        else:
            score += 40*(myNbPieces - OpponentNbPieces)
       
        self.totalPieces = myNbPieces + OpponentNbPieces
        
        # Reduce mobility strategy
        (self.myNbMoves,self.oppNbMoves) = self.getNbMoves()
        score += 70 * (self.myNbMoves - self.oppNbMoves)
        
        
        return score

    
    def negascout(self,depth,alpha,beta):
        if (self._board.is_game_over() | depth <= 0):
            return self.evaluation()
        betaprim = beta
        firstChild = True
        for move in self._board.legal_moves():
            self._board.push(move)
            v = - self.negascout(depth-1, -betaprim, -alpha)
            self._board.pop()
            if ( (alpha < v < beta) and (firstChild) and (not self._board.is_game_over())):
                self._board.push(move)
                alpha = - self.negascout(depth-1, -beta, -v)
                self._board.pop()
                firstChild = False
            alpha = max(alpha,v)
            if (alpha >= beta):
                return alpha
            betaprim = alpha + 1
        return alpha


    def getNbMoves(self):
        oppNbMoves = 0
        myNbMoves = 0
        for x in range(0,self._board.get_board_size()):
            for y in range(0,self._board.get_board_size()):
                if self._board.is_valid_move( self._opponent, x, y):
                    oppNbMoves +=1  
                if self._board.is_valid_move( self._mycolor, x, y):
                    myNbMoves += 1  
        return (myNbMoves,oppNbMoves)
