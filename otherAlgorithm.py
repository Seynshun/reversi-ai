import Reversi
import math

''' nega alpha beta en remontant les raisons de l'echec pour remonter la meilleure valeur
des fils peu importe les valeurs initiales de alpha beta'''

def negEchecAlphaBeta(self,depth,alpha,beta):
    if (self._board.is_game_over() | depth <= 0):
        return self.evaluation()
    best = -math.inf
    for move in self._board.legal_moves():
        self._board.push(move)
        v = - negEchecAlphaBeta(self,depth-1, -beta, -alpha)
        self._board.pop()
        if v > best:
            best = v
            if best > alpha:
                alpha = best
                if alpha >= beta:
                    return best
    return best

def negastar(self,depth,alpha,beta):
    if (self._board.is_game_over() | depth <= 0):
        return self.evaluation()
    minus = -math.inf
    plus = math.inf
    while minus != plus :
        v = (minus + plus)/2
        t = negEchecAlphaBeta(self,depth-1,v,v+1)
        if t > v:
            minus = t
        else :
            plus = t
    return minus


def MinMax(self, depth, player):
    if self._board.is_game_over():
        return self.evaluation()
    if depth <= 0:
        return self.evaluation()

    if player:
        v = -math.inf
        for move in self._board.legal_moves():
            self._board.push(move)
            v = max(v, MinMax(b, depth-1, False))
            self._board.pop()
    else:
        v = math.inf
        for move in self._board.legal_moves():
            self._board.push(move)
            v = min(v, MinMax(b, depth-1, True))
            self._board.pop()

    return v

def negamaxAlphaBeta(self,depth, alpha, beta):
    if (self._board.is_game_over() | depth <= 0):
        return self.evaluation()
    v = -math.inf
    for move in self._board.legal_moves():
        self._board.push(move)
        v = max(v,- self.negamaxAlphaBeta(depth -1, -beta, -alpha))
        self._board.pop()
        alpha = max(alpha, v)
        if(alpha >= beta):
            break            
    return v


def mtdf(self,g,d):
    best = g
    upper = math.inf
    lower = -math.inf
    while lower < upper:
        beta = max(best,lower+1)
        best = negEchecAlphaBeta(self,d,beta-1,beta)
        if best < beta:
            upper = best
        else :
            lower = best
    return best

