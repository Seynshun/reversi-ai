# Reversi AI

AI for Reversi/Othello board game for the Artificial Intelligence course project of Master 1 of the University of Bordeaux.
Displays the confrontation of two 2 AI on terminal or with graphical interface.
<p align="center">
<img alt="terminal" src="https://zupimages.net/up/20/36/xsgf.png" height="300" />
<img alt="graphical interface" src="https://zupimages.net/up/20/36/s5fh.png" height="300"/>
</p>




# Run

### Requirements

 - python 3
 - pygame
 - tkinter  (for graphical interface)

### Run on terminal
```shell
python3 localGame.py
```
### Run with graphical interface
```shell
python3 localGameGUI.py
```
# AI Strategy

The game tree is visited with the "Negascout" Algorithm. Other tested algorithms are present in [otherAlgortihm.py](https://gitlab.com/Seynshun/reversi-ai/-/blob/master/otherAlgorithm.py).

Heuristics uses the followings strategies :

 - Position evaluation : A score is attributed according to the importance of the position : corners and edges give more points. Positions that allows opponent to obtains edges or corners should be avoided.
 
 - "Evaporation" strategy : This strategy consist to minimize the number of pieces at the beginning of the game to increase the mobility and get more pieces at the end.
 
 - Reduction of the opponent's mobility :  Whenever possible, pieces are placed in such a way that the opponent has as few choices as possible.
 
 - "Stability" strategy :  If a corner is captured, we know that the piece can't be captured by the opponent and the adjacent pieces can't be captured either, so we changed the previous position evaluation to favor the propagation form a corner. 

